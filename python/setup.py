from distutils.core import setup
from Cython.Build import cythonize
import os

#TODO: this is hacky, can we use extra_link_args?: https://github.com/cython/cython/wiki/WrappingCPlusPlus
os.environ['CFLAGS'] = '-lm -L/usr/local/lib -lGevApi -lCorW32'

setup(ext_modules = cythonize(
           "py_dalsa_camera.pyx",                 # our Cython source
           language="c++",             # generate C++ code
     ))