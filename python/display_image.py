import numpy as np
import matplotlib.pyplot as plt


img = np.loadtxt(open("img.csv", "rb"), delimiter=",")
img = img/255.0

#plt.imshow(img, cmap='Greys')

plt.imsave("img_greys.png", img, cmap='Greys')

#plt.show()