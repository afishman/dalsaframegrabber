import py_dalsa_camera
import matplotlib.pyplot as plt
import numpy as np

width = 2560/2;
height = 2048/2;

cam = py_dalsa_camera.PyDalsaCamera()
cam.open(width, height, 5)

img = cam.get_next_image()

img_int = []

for i in range(width*height):
	img_int.append(ord(img[i]))

reshaped = np.array(img_int).reshape(height, width)


plt.imshow(reshaped, cmap='gray')
plt.show()