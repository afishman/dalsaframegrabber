# distutils: language = c++
# distutils: sources = dalsa_camera.cpp
# distutils: include_dirs = /usr/dalsa/GigeV/include/

ctypedef unsigned char UINT8

cdef extern from "dalsa_camera.h":
	cdef cppclass dalsa_camera:
		dalsa_camera() except +
		int open(int width, int height, float framerate)
		int get_next_image(UINT8** img)
	
	
cdef class PyDalsaCamera:
	cdef dalsa_camera c_camera

	def __cinit__(self):
		self.c_camera = dalsa_camera()

	def open(self, width, height, framerate):
		self.c_camera.open(width, height, framerate)

	def get_next_image(self):
		cdef UINT8* img
		self.c_camera.get_next_image(&img)

		return img